/*
 * The MIT License
 *
 * Copyright 2014 Chris Skevis <skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package PROVIDERS;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Chris Skevis <skevis.org>
 */
public class PoolProvider {
    //SOme enumerations for Algorithms
    public static final int SCRYPTN = 0;
    public static final int SCRYPT = 1;
    public static final int X11 = 2;
    public static final int SHA256 = 3;
    
    private String poolDescription = null;
    private String poolURL = null;
    private String poolMainPageURL = null;
    private String addressMonitorUrl = null;
    private int poolAlgorithm;
    //should be 1 to 3
    private int P2poolNumber = 3;
    private final String vertGeekNzAPIScheme = "http";
    private final String vertGeekNzAPIURL = "vert.geek.nz";
    private final String vertGeekNzAPIPath = "/api/nodefinder.php";
    
    
    /** Default Constructor of poolProvider.
    *  <ul> 
    *  <li>poolDescription is "Closest P2Pool"</li> 
    *  <li>poolURL is The Closest network 3 P2Pool</li> 
    *  </ul>
    * 
    * @since    0.01
    */ 
    public PoolProvider(){
        this.poolDescription = "P2Pool";
        this.poolURL = getClosestP2POOLNode(P2poolNumber);
        this.poolAlgorithm = SCRYPTN;
        this.addressMonitorUrl = this.poolURL;
        this.poolMainPageURL = this.poolURL;
    }
    
    /** CUstom Pool Constructor of poolProvider.
    * This Pool will start by considering that the monitor address 
    * is the same as the pools
    *  <ul> 
    *  <li>poolDescription set by user</li> 
    *  <li>poolURL set by user</li> 
    *  </ul>
    * 
    * Will Fallback to closest network 3 P2Pool if custom Pool Dead
    * @param poolDescription A small description for the pool
    * @param poolURL         The full url and of the pool (including http:// and :port)
    * @param poolAlgorithm   The algorithm that this pool can mine.
    * 
    * @since    0.01
    */ 
    public PoolProvider(String poolDescription, String poolURL, int poolAlgorithm){

        if(checkIfURLAlive(poolURL)){
            this.poolDescription = poolDescription;
            this.poolURL = poolURL;
            this.poolAlgorithm = poolAlgorithm;
            this.addressMonitorUrl = this.poolURL;
            this.poolMainPageURL = this.poolURL;
        }
        else{
            this.poolDescription = poolURL + " seems dead, using";
            this.poolURL = getClosestP2POOLNode(P2poolNumber);
            this.poolAlgorithm = SCRYPTN;
            this.addressMonitorUrl = this.poolURL;
            this.poolMainPageURL = this.poolURL;
        }
    }
    
    /** CUstom Pool Constructor of poolProvider.
    *  <ul> 
    *  <li>poolDescription set by user</li> 
    *  <li>poolURL set by user</li> 
    *  </ul>
    * 
    * Will Fallback to closest network 3 P2Pool if custom Pool Dead
    * @param poolDescription A small description for the pool
    * @param poolURL         The full url and of the pool (including http:// and :port)
    * @param poolAlgorithm   The algorithm that this pool can mine.
    * @param addressMonitorUrl The URL the user must go to in order to view his mining status
    * 
    * @since    0.01
    */ 
    public PoolProvider(String poolDescription, String poolURL, int poolAlgorithm, String addressMonitorUrl, String mainPageURL){

        if(checkIfURLAlive(mainPageURL)){
            this.poolDescription = poolDescription;
            this.poolURL = poolURL;
            this.poolAlgorithm = poolAlgorithm;
            this.addressMonitorUrl = addressMonitorUrl;
            this.poolMainPageURL = mainPageURL;
        }
        else{
            this.poolDescription = poolURL + " seems dead, using";
            this.poolURL = getClosestP2POOLNode(P2poolNumber);
            this.poolAlgorithm = SCRYPTN;
            this.addressMonitorUrl = this.poolURL;
            this.poolMainPageURL = this.poolURL;
        }
    }
    
    /**
    * Returns true if the URL is open for connections. 
    * The network argument must be a number from 1 to 3. 
    * <p>
    * This method checks if a URL is Alive by getting the HEAD part of the
    * connection and testing the HTTP_OK(200) or HTTP_MOVED_TEMP (302) status.
    * It times out the connection on 1 second by default 
    *
    * @param    url A URL including the port number
    * @return       <true> URL is ok <false> URL is dead
    * @since    0.01
    */
    private boolean checkIfURLAlive(String URLToTest){
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(URLToTest).openConnection();
            //1 Second should be enough, if more we dont want this host anyway
            con.setConnectTimeout(2 * 1000);
            con.setRequestMethod("HEAD");
            
            //HTTP 302 or 200 are ok to consider as alive
            return (con.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) || (con.getResponseCode() == HttpURLConnection.HTTP_OK);
          }
          catch (IOException e) {
             System.out.println(e.getMessage());
             return false;
          }
    }
    
    /**
    * Returns the closest P2Pool IP or hostname based on current user's IP. 
    * The network argument must be a number from 1 to 3. 
    * <p>
    * This method uses the network argument in order to find a P2Pool near the 
    * user, based on geolocation of his IP. The network argument is optional as 
    * the program will auto adjust in order to fit the user PC cababillities.
    * It uses the vert.geel.nz API and Apache HttpCLient Lib to find the closest
    * alive P2Pools but since it can return dead pools 
    * (vert.geek.nz checks every 20 minutes for that) it also checks the list 
    * of results to find the closest alive P2Pool.
    *
    * @param    network a number from 1 to 3 to specify Pool difficulty
    * @return           the url connection String of the closest Pool
    * 
    * @since    0.01
    */
    private String getClosestP2POOLNode(int network){
        //If the network Value is not set, initiallize it at 3 as safer option
        if(network == 0)
            network = getP2poolNumber();
            
        //Get the Closest Node
        String candidatePool = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            URI vertGeeksUri = new URIBuilder().setScheme(vertGeekNzAPIScheme).setHost(vertGeekNzAPIURL).setPath(vertGeekNzAPIPath)
                    .setParameter("results", "20").setParameter("network", Integer.toString(network)).build();
            HttpGet httpget = new HttpGet(vertGeeksUri);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };
            String responseBody = null;
            try {
                responseBody = httpclient.execute(httpget, responseHandler);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject;
            try {
                jsonObject = (JSONObject) jsonParser.parse(responseBody);
                JSONArray nodes= (JSONArray) jsonObject.get("nodes");
                for (Object node : nodes) {
                    
                    JSONObject jsonObject2 = (JSONObject) node;
                    candidatePool = (String) jsonObject2.get("url");
                    
                    if(checkIfURLAlive(candidatePool)){
                        break;
                    }
                }
            } catch (ParseException ex) {
                System.out.println(ex.getMessage());
            }    
        } 
        catch(URISyntaxException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                httpclient.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        return candidatePool;
    }
    
    /**
    * Selects one P2Pool address based on the current P2PoolNumber value 
    * <p>
    * This method should be used after a set P2PoolNumber is called 
    * so that we get a new P2Pool ip selected on this provider
    * OR if we just need to recheck for dead pools
    *
    * @since    0.01
    */
    public void ShuffleP2Pool(){
        this.poolURL = getClosestP2POOLNode(this.P2poolNumber);
    }

    /**
     * @return the poolDescription
     */
    public String getPoolDescription() {
        return poolDescription;
    }

    /**
     * @return the poolURL
     */
    public String getPoolURL() {
        return poolURL;
    }

    /**
     * @param poolDescription the poolDescription to set
     */
    public void setPoolDescription(String poolDescription) {
        this.poolDescription = poolDescription;
    }

    /**
     * @param poolURL the poolURL to set
     */
    public void setPoolURL(String poolURL) {
        this.poolURL = poolURL;
    }

    /**
     * @return the P2poolNumber
     */
    public int getP2poolNumber() {
        return P2poolNumber;
    }

    /**
     * @param P2poolNumber the P2poolNumber to set
     */
    public void setP2poolNumber(int P2poolNumber) {
        this.P2poolNumber = P2poolNumber;
    }
    
    /**
     * @return the poolAlgorithm
     */
    public int getPoolAlgorithm() {
        return poolAlgorithm;
    }

    /**
     * @param poolAlgorithm the poolAlgorithm to set
     */
    public void setPoolAlgorithm(int poolAlgorithm) {
        this.poolAlgorithm = poolAlgorithm;
    }

    /**
     * @return the addressMonitorUrl
     */
    public String getAddressMonitorUrl() {
        return addressMonitorUrl;
    }

    /**
     * @param addressMonitorUrl the addressMonitorUrl to set
     */
    public void setAddressMonitorUrl(String addressMonitorUrl) {
        this.addressMonitorUrl = addressMonitorUrl;
    }
    
        /**
     * @return the poolMainPageURL
     */
    public String getPoolMainPageURL() {
        return poolMainPageURL;
    }

    /**
     * @param poolMainPageURL the poolMainPageURL to set
     */
    public void setPoolMainPageURL(String poolMainPageURL) {
        this.poolMainPageURL = poolMainPageURL;
    }
    
    @Override
    public String toString(){
        return poolDescription + ": " + poolURL;
    }
    
}
