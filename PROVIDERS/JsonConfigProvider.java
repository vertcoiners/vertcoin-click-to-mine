/*
 * The MIT License
 *
 * Copyright 2014 Chris Skevis <skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package PROVIDERS;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 *
 * @author Chris Skevis <skevis.org>
 */
public class JsonConfigProvider {
    
    private String fullJSON = null;
    private final String jsonFIle = "/RESOURCES/verters_configs.json";
    String[] manufacturersArray = null;
    String[] manufacturersGPUArray = null;
    
    public JsonConfigProvider(){
        this.fullJSON = getJSONFromFile(jsonFIle);
        setManufacturersArray();
        setManufacturerGPU(0);
    }
    
    private String getJSONFromFile(String Filename){
        String everything = "";
        
        try {
            StringBuilder sb;
            try (BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(jsonFIle)))) {
                sb = new StringBuilder();
                String line = br.readLine();
                while (line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
            }
            everything = sb.toString();
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        
        return everything;
    }
    
    public void setManufacturerGPU(int manufacturerIndex){      

        JsonElement jelement = new JsonParser().parse(fullJSON);
        JsonObject  jobject = jelement.getAsJsonObject();

        int i = 0;
        int j = 1;
        for (Map.Entry<String,JsonElement> entry : jobject.entrySet()) {
            if(i == manufacturerIndex){
                
                JsonObject  jobject2 = entry.getValue().getAsJsonObject();
                manufacturersGPUArray = new String[jobject2.entrySet().size()+1];
                manufacturersGPUArray[0] = "Other";
                
                for (Map.Entry<String,JsonElement> entry2 : jobject2.entrySet()) {
                    this.manufacturersGPUArray[j++] = entry2.getKey();
                }
                
                break;
            }
            i++;
        }
        
    }
    
    public String getModelConfig(int manufacturerIndex, int GPU){      

        JsonElement jelement = new JsonParser().parse(fullJSON);
        JsonObject  jobject = jelement.getAsJsonObject();
        String config = null;
        
        int i = 0;
        int j = 0;
        for (Map.Entry<String,JsonElement> entry : jobject.entrySet()) {
            if(i == manufacturerIndex){
                
                JsonObject  jobject2 = entry.getValue().getAsJsonObject();
                
                for (Map.Entry<String,JsonElement> entry2 : jobject2.entrySet()) {
                    if(j == GPU ){                        
                        JsonArray  jobject3 = entry2.getValue().getAsJsonArray();
                        
                        jobject = jobject3.get(0).getAsJsonObject();
                        config = jobject.get("config").toString();
                        
                        break;
                    }
                    j++;    
                }
                
                break;
            }
            i++;
        }
        
        return config;
    }
    
    public void setManufacturersArray(){
        JsonElement jelement = new JsonParser().parse(fullJSON);
        JsonObject  jobject = jelement.getAsJsonObject();

        int i = 1;
        this.manufacturersArray = new String[jobject.entrySet().size()+1];
        this.manufacturersArray[0] = "Other";
        for (Map.Entry<String,JsonElement> entry : jobject.entrySet()) {
            this.manufacturersArray[i++] = entry.getKey();
        }
    }
    
    public String[] getManufacturersArray(){
        return this.manufacturersArray;
    }
    
    public String[] getManufacturersGPUArray(){
        return this.manufacturersGPUArray;
    }

    /**
     * @return the fullJSON
     */
    public String getFullJSON() {
        return fullJSON;
    }

    /**
     * @param fullJSON the fullJSON to set
     */
    public void setFullJSON(String fullJSON) {
        this.fullJSON = fullJSON;
    }
}
